-- set the concurrent task limit for a specified queue and user
INSERT INTO EGICM_CONCURRENT_TASK
(USER_ID, QUEUE_ID, CONCURRENT_TASK_LIMIT)
VALUES (%d, %d, %d)
