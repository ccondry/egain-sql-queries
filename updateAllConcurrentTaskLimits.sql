-- Update the number of concurrent tasks that all users are allowed on the given
-- queue. This will only affect users who are not at the default number of
-- concurrent tasks for this queue.

UPDATE EGICM_CONCURRENT_TASK
SET CONCURRENT_TASK_LIMIT = %d
WHERE QUEUE_ID = %d
-- use this next line to only increase task limits, and avoid decreasing task
-- limits for users with a higher limit already set
AND CONCURRENT_TASK_LIMIT < %d
