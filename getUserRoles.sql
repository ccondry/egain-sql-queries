-- Retrieve the user roles for the given department.
-- In the ECE Administration interface, this is (department)>Users>Roles
SELECT [ROLE_ID]
,[ROLE_NAME]
,[ROLE_DESCRIPTION]
FROM [eGActiveDB].[dbo].[EGPL_USER_ROLE]
WHERE Department_id = %d
