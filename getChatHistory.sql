-- use the email address of the contact to pull chat history for a single
-- customer.
-- You could also just pull all history by removing that EMAIL_ADDRESS
-- condition in the WHERE clause.

--the contact's email address
DECLARE @email int = %s;
-- the ECE department
DECLARE @department int = %d;

SELECT p.FIRST_NAME
      ,p.LAST_NAME
      ,act.[ACTIVITY_ID]
      ,[CASE_ID]
      ,[ACTIVITY_SUB_TYPE]
      ,[ACTIVITY_STATUS]
      ,[ACTIVITY_SUB_STATUS]
      ,act.[WHEN_CREATED]
      ,act.[WHEN_MODIFIED]
      ,[SUBJECT]
      ,[LANGUAGE_ID]
      ,[QUEUE_ID]
	  ,s.[CONTENT]
	  ,icm.[ICM_AGENT_ID]
      ,icm.[SKILL_GROUP_ID]
	  ,eguser.[SCREEN_NAME] AS AGENT_SCREEN_NAME
	  ,eguser.USER_NAME AS AGENT_USER_NAME
	  ,eguser.FIRST_NAME AS AGENT_FIRST_NAME
	  ,eguser.LAST_NAME AS AGENT_LAST_NAME
  FROM [eGActiveDB].[dbo].[EGPL_CASEMGMT_ACTIVITY_9000] act
  JOIN [eGActiveDB].[dbo].[EGPL_CASEMGMT_CONTACT_POINT] cp ON cp.CONTACT_POINT_ID = act.CONTACT_POINT_ID
  JOIN [eGActiveDB].[dbo].[EGPL_CASEMGMT_CPOINT_EMAIL] cpe on cpe.CONTACT_POINT_ID = act.CONTACT_POINT_ID
  JOIN [eGActiveDB].[dbo].[EGPL_CASEMGMT_CONTACT_PERSON] p ON p.CUSTOMER_ID = cp.CUSTOMER_ID
  JOIN [eGActiveDB].[dbo].[EGLV_SESSION_CONTENT] s ON s.ACTIVITY_ID = act.ACTIVITY_ID
  JOIN [eGActiveDB].[dbo].[EGICM_ACTIVITY] icm ON icm.ACTIVITY_ID = act.ACTIVITY_ID
  JOIN [eGActiveDB].[dbo].[EGICM_USER] icmuser ON icmuser.SKILL_TARGET_ID = icm.ICM_AGENT_ID
  JOIN [eGActiveDB].[dbo].[EGPL_USER] eguser ON eguser.USER_ID = icmuser.USER_ID
  WHERE cpe.DEPARTMENT_ID = @department
  -- remove next line to get chat history for all contacts (may be a large result set!)
  AND EMAIL_ADDRESS = @email
  AND p.DELETE_FLAG = 'n'
  AND ACTIVITY_TYPE = 2000
