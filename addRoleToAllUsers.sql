-- Assign a role to every user in a department
-- Users who already have this role will be unaffected.

-- set role ID here
DECLARE @role_id int = %d;
-- set your department here
DECLARE @department int = %d;
INSERT INTO [EGPL_USER_PARTY_ROLE] ([PARTY_ID], [ROLE_ID])
SELECT [USER_ID], @role_id AS ROLE_ID
FROM [EGPL_USER]
WHERE [DEPARTMENT_ID] = @department
-- don't add a role that the user already has
AND [USER_ID] NOT IN (
	SELECT [USER_ID]
	FROM [EGPL_USER] c
	JOIN [EGPL_USER_PARTY_ROLE] d ON c.USER_ID = d.PARTY_ID
	WHERE [DEPARTMENT_ID] = @department
	AND [ROLE_ID] = @role_id
)
