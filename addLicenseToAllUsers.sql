-- Assign a license to every user in a department
-- Users who already have this license will be unaffected

--use the PKEY, not the LICENSE_ID column that is also in that table
DECLARE @license int = %d
--your ECE epartment ID. 999 is the first/default department created
DECLARE @department int = %d
--run this query for each license you want to add to every user in the department
INSERT INTO [EGPL_LICENSE_USER_ASSIGNMENT] (USER_ID,LICENSE_KEY)
SELECT USER_ID, @license AS LICENSE_KEY
FROM EGPL_USER
WHERE DEPARTMENT_ID = @department
-- don't add a license that the user already has
AND USER_ID NOT IN (
	SELECT USER_ID
	FROM [EGPL_LICENSE_USER_ASSIGNMENT]
	WHERE LICENSE_KEY = @license
)
