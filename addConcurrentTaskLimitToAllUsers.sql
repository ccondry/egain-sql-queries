-- Increase/change the number of concurrent tasks that all users are allowed on
-- the given queue. This will not affect users who are not at the default number
-- of concurrent tasks for this queue;
-- for that, use updateConcurrentTaskLimitOnAllUsers()

DECLARE @queue int = %d; --the queue ID to update
DECLARE @tasks int = %d; --the number of concurrent tasks
DECLARE @department int = %d; -- the ECE department
INSERT INTO EGICM_CONCURRENT_TASK(QUEUE_ID,USER_ID,CONCURRENT_TASK_LIMIT)
SELECT @queue QUEUE_ID,USER_ID,@tasks CONCURRENT_TASK_LIMIT
FROM EGPL_USER
WHERE USER_ID IN (
	SELECT [USER_ID]
	FROM EGPL_USER
	WHERE DEPARTMENT_ID = @department
)
AND USER_ID NOT IN (
	SELECT USER_ID
	FROM EGICM_CONCURRENT_TASK
	WHERE QUEUE_ID = @queue
)
